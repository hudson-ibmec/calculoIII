<center> <h3> <b> OTIMIZAÇÃO E PROCESSOS DINÂMICOS </b> </h3> </center> 

Neste repositório você encontrará informações e códigos necessários para gerar o material da disciplina Otimização e Processos Dinâmicos ministrada no curso de Ciências Econômicas do [Ibmec-MG](https://www.ibmec.br/mg).

Ao final da disciplina o aluno estará apto a resolver problemas de Macroeconomia e Microeconomia tanto estáticos quanto dinâmicos. Os tópicos discutidos são os seguintes:

* [Introdução](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/1.introducao/aula1.pdf) 
    * Economia Matemática e Economia Não-Matemática 
    * Modelo Matemático/Modelo Econômico x Modelo Econométrico 
* Programação em R 
    * Como usar as plataformas DataCamp e RStudio Cloud para aprender o R  
    * [Introdução: Aritmética, Variáveis, Vetores, Fatores, Matrizes, Data Frames e Listas](https://rpubs.com/hudsonchavs/introducaoR)
    * [Intermediário: Operadores Relacionais, Condições, Loops, Funções, Datas e Pacotes](https://rpubs.com/hudsonchavs/intermediarioR)
    * [Importar dados locais (csv, xlsx, txt) e da internet (csv, xlsx, txt, APIs)](https://rpubs.com/hudsonchavs/importardadosR)
    * [Coletar dados financeiros e econômicos de fontes públicas como o Banco Central do Brasil, BM&F Bovespa, IBGE, IPEA, etc](https://rpubs.com/hudsonchavs/coletardadosR)
* Otimização Estática
    * [Revisão principais conceitos de otimização estática](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/2.otimizacao/1.estatica/1.revisao/revisao_otimizacao.pdf)
    * [Otimização Não-Condicionada com N variáveis de escolha](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/2.otimizacao/1.estatica/2.n_variaveis/otimizacao_nao_condicionada.pdf)
    * [Otimização Condicionada com N variáveis de escolha e M restrições de igualdade](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/2.otimizacao/1.estatica/2.n_variaveis/otimizacao_condicionada.pdf)
    * Côncavidade, Convexidade, Quase-Côncavidade e Quase-Convexidade
    * [Programação Linear](https://rpubs.com/hudsonchavs/linearprogramming)
    * [Programação Não-Linear (condições de Karush-Kuhn-Tucker para problemas com restrições de desigualdade)](https://rpubs.com/hudsonchavs/nonlinearprogramming)
* Dinâmica
    * [Equações Diferenciais](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/3.equacoes_diferenciais/equacoes_diferenciais.pdf)
    * [Funções Circulares](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/3.equacoes_diferenciais/funcoes_circulares.pdf)
    * [Equações em Diferenças](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/4.equacoes_em_diferencas/equacoes_em_diferencas.pdf)
    * [Sistemas Dinâmicos](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/5.sistemas/sistemas_dinamicos.pdf)
* Otimização Dinâmica
    * [Controle Ótimo](https://gitlab.com/hudson-ibmec/calculoIII/blob/master/notes/6.contole_otimo/controle_otimo.pdf)
* Aplicações em Economia
    * Modelo de Solow  

### **ESTRUTURA DE PASTAS DESTE REPOSITÓRIO**

Para facilitar o entendimento, os arquivos estão divididos em:

* `notes`: pasta com os códigos que geram as notas de aula
* `homework`: pasta com códigos que geram as listas de exercícios e trabalhos

