---
title: \large{\textbf{OTIMIZAÇÃO E PROCESSOS DINÂMICOS - RESPOSTAS DA LISTA 4}}
author: \scriptsize{Hudson Chaves Costa}
header-includes:
   - \usepackage{titling}
   - \pretitle{\begin{center}\small\includegraphics[height=1.2cm]{../../figures/ibmecLogo.png}\\[\bigskipamount]}
   - \posttitle{\end{center}}
urlcolor: blue
output: 
  pdf_document
---

```{r, echo=TRUE, eval=FALSE}
################################
####        VETORES        #####
################################

# Exercício 1
x = c(4,6,5,7,10,9,4,15)
x < 7

# Exercício 2
p = c(3, 5, 6, 8)
q = c(3, 3, 3)
p+q

# Exercício 3
x = c(1,2,3,4)
(x+2)[(!is.na(x)) & x > 0] -> k

# Exercício 4
x = c(2, 4, 6, 8)
y = c(TRUE, TRUE, FALSE, TRUE)
sum(x[y])

# Exercício 5
x = c(34, 56, 55, 87, NA, 4, 77, NA, 21, NA, 39)
sum(is.na(x))

# Exercício 6
x = c(4,6,5,7,10,9,4,15)
y = c(0,10,1,8,2,3,4,1)
x*y

# Exercício 7
a = c(1,2,4,5,6)
b = c(3,2,4,1,9)
cbind(a,b)

# Exercício 8
a = c(1,5,4,3,6)
b = c(3,5,2,1,9)
a <= b

################################
####        FATORES        #####
################################

# Exercício 1
x = c(1, 2, 3, 3, 5, 3, 2, 4, NA)
levels(factor(x))

# Exercício 2
x = c(11, 22, 47, 47, 11, 47, 11)
factor(x, levels=c(11, 22, 47), ordered=TRUE)

# Exercício 3
s1 = factor(sample(letters, size=5, replace=TRUE))
s2 = factor(sample(letters, size=5, replace=TRUE))
factor(c(levels(s1)[s1], levels(s2)[s2]))

# Exercício 4
x = factor(c("high", "low", "medium", "high", "high", "low", "medium"))
data.frame(levels = unique(x), value = as.numeric(unique(x)))

################################
####        MATRIZES       #####
################################

# Exercício 1
M = matrix(c(1:10),nrow=5,ncol=2,dimnames=list(c('a','b','c','d','e'),c('A','B')))
M[1,]
M[,1]
M[3,2]
M['e','A']

# Exercício 2
A + B
A - B

# Exercício 3
b = c(7,4)
b%*%A

# Exercício 4
t(A)

# Exercício 5
solve(A)

# Exercício 6
det(B)

################################
####      DATA FRAMES      #####
################################

# Exercício 1
Name = c("Alex", "Lilly", "Mark", "Oliver", "Martha", "Lucas", "Caroline")
Age = c(25, 31, 23, 52, 76, 49, 26)
Height = c(177, 163, 190, 179, 163, 183, 164)
Weight = c(57, 69, 83, 75, 70, 83, 53)
Sex = as.factor(c("F", "F", "M", "M", "F", "M", "F"))
df = data.frame (row.names = Name, Age, Height, Weight, Sex)
levels(df$Sex) = c("M", "F")
df

# Exercício 2
Name = c("Alex", "Lilly", "Mark", "Oliver", "Martha", "Lucas", "Caroline")
Working = c("Yes", "No", "No", "Yes", "Yes", "No", "Yes")
dfa = data.frame(row.names = Name, Working)
dfa

# Exercício 3
dfa = cbind(df,dfa)

# Exercício 4
dim(dfa)

# Exercício 5
sapply(dfa, class)


################################
####        LISTAS         #####
################################

# Exercício 1
p = c(2,7,8)
q = c("A", "B", "C")
x = list(p, q)
x[2]

# Exercício 2
w = c(2, 7, 8)
v = c("A", "B", "C")
x = list(w, v)
x[[2]][1] = "K"

# Exercício 3
a = list ("x"=5, "y"=10, "z"=15)
sum(unlist(a))

# Exercício 4
year = c(2005:2016)
month = c(1:12)
day = c(1:31)

Date = list()
Date$year = year
Date$month = month
Date$day = day
Date
```

```{r, echo=TRUE, eval=FALSE}
################################
####       CONDIÇÕES       #####
################################

# Exercício 1
x = -10
abs = x
if (x < 0) {
  abs = -x
}
cat("O valor absoluto de", x, "é", abs , "\n" )

# Exercício 2
x = 16
y = ifelse(x >= 0, x, NA)
cat("A raiz quadrada de",  x, "é", sqrt(y))

# Exercício 3
x = c(10, 1)
if(x[1] > x[2]) {
  cat("O valor máximo é", x[1], "\n")
} else cat("O valor máximo é", x[2], "\n")

# Exercício 4
x = c(-100, 10, 20, 30, 50, 51, 52, 53, 54, 55)
counter = 0
mean = mean(x)

for (i in 1:length(x)){
  if(x[i] > mean){
    counter = counter +1
  }
}
cat("O número de valores que são maiores que a média é", counter, "\n")

# Exercício 5
x = c(30, 120, 100)

if (x[1] > x[2]){
  fir = x[1]
  sec = x[2]
} else {
  fir = x[2]
  sec = x[1]
}
if ( x[3] > fir & x[3] > sec ) {
  thi = sec
  sec = fir
  fir = x[3]
} else if ( x[3] < fir & x[3] < sec ) {
  thi = x[3]
} else {
  thi = sec
  sec = x[3]
}
cat (fir, sec, thi, "\n")

# Exercício 6
ifelse(sqrt(9)<2,sqrt(9),0)

################################
####        LOOPS          #####
################################

# Exercício 1
msg = c("Hello")
i = 1
while (i < 7) {
  print(msg)
  i = i + 1
}

# Exercício 2
x = c(7, 4, 3, 8, 9, 25)

for(i in 1:4) {
  print(x[i])
}

# Exercício 3
y = c("q", "w", "e", "r", "z", "c")

for(letter in y) {
  print(letter)
}

################################
####      FUNÇÕES          #####
################################

# Exercício 1
f.sum = function (x, y) {
  r = x + y
  r
}
f.sum(5, 10)

# Exercício 2
f.exists = function (v, x) {
  exist = FALSE
  i = 1
  
  while (i <= length (v) & !exist) {
    
    if (v[i] == x) {
      exist = TRUE
    }
    i = 1 + i
  }
  exist
}
f.exists(c(1:10), 10)

# Exercício 3
f.count = function (v, x) {
  count = 0
  
  for (i in 1:length(v)) {
    if (v[i] == x) {
      count = count + 1
    }
  }
  count
}
f.count(c(1:9, rep(10, 100)), 10)
```



